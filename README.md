# ChairTable initial draft steps (Procedural)

## Forming Table Component
1. Create the table base 
	a. Have minimum limits for table
2. Function to create Table Leg component
3. Form the table structure by creating 1 base and 4 Legs on specific location. (Base corners)

## Forming Chair
1. Create a Chair component

## Table Chair
1. Spwan the Chair and Table procedurally on the Floor component on specific location
2. Spawn chair between Table Leg space

## Drag event
1. Conroller to hadle the Drag
2. Place Drag points on the corners of the table
3. Handle mouse X and Y Drag and add Chair or remove chair based on size
