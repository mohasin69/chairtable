#pragma once

class UDragAndDropComponent;

/*
*
*/
class IDragAndDropComponent
{
public:
    virtual void OnChangeAlongX(UDragAndDropComponent* Dragger, float DeltaX) = 0;
    virtual void OnChangeAlongY(UDragAndDropComponent* Dragger, float DeltaY) = 0;
};