// Fill out your copyright notice in the Description page of Project Settings.


#include "ProcuduralRoom.h"
#include "MyChairPoolComponent.h"
#include "MyChair.h"

// Sets default values
AProcuduralRoom::AProcuduralRoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    //ChairPool = CreateDefaultSubobject<UMyChairPoolComponent>(TEXT("ChairPool"));

	Floor = CreateDefaultSubobject<UStaticMeshComponent>( TEXT("FloorComponent"));

    //ChairMeshOrigin = AMyChair::GetChairMeshOrigin();
    //ChairMeshExtent = AMyChair::ChairMeshExtent;
    //LengthChairWithMargins = (ChairMeshExtent.Y * 2) + (MarginChair * 2);

	SetRootComponent(Floor);

}

// Called when the game starts or when spawned
void AProcuduralRoom::BeginPlay()
{
	Super::BeginPlay();
	//SpawnItem(TableClass);
	UpdateChairs();
}

void AProcuduralRoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AProcuduralRoom::Update()
{
	Super::Update();

	//UpdateChairs();
}
void AProcuduralRoom::SpawnItem(UClass* SpwanItemClass)
{
	GetWorld()->SpawnActor<AActor>(SpwanItemClass, FVector(0.f), FRotator(0.f));
}