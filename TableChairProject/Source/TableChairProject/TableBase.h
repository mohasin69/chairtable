// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "DragAndDropComponent.h"
#include "IDragAndDropComponent.h"
#include "TableBase.generated.h"

class UDragAndDropComponent;
class AMyChair;
class UMyChairPoolComponent;

UCLASS()
class TABLECHAIRPROJECT_API ATableBase : public AActor, public IDragAndDropComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATableBase();
	FORCEINLINE FVector GetLocalBoundsMax() const { return LocalBoundsMax; }
	void UpdateLocalBoundsMax(FVector NewLocalBoundsMax);

	FORCEINLINE FVector GetLocalBoundsMin() const { return LocalBoundsMin; }
	void UpdateLocalBoundsMin(FVector NewLocalBoundsMin);

	void UpdateTableTopMesh();

	virtual void Update();

	const float TableSizeLowerLimitX = 100.0f;
	const float TableSizeLowerLimitY = 100.0f;
	const float TableSizeLowerLimitZ = 10.0f;

	const float TableHeight = 65.0f;


	const float TableLegSizeLowerLimitX = 10.0f;
	const float TableLegSizeLowerLimitY = 10.0f;
	const float TableLegSizeLowerLimitZ = TableHeight;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh parameters")
		FVector CubeRadius = FVector(TableSizeLowerLimitX, TableSizeLowerLimitY, TableSizeLowerLimitZ);
	static const FString LegMeshPath;

protected:
	const int32 NumLegs = 4;
	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisScene;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisTableMesh;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisTableLegMesh[4];

	virtual void PostActorCreated() override;
	virtual void PostLoad() override;

	void Generatemesh(bool updateTheMesh = false);
	void GenerateTableLeg();

	TArray<UStaticMeshComponent*> TableLegMeshes;
	FVector TableLegMeshOrigin;
	FVector TableLegMeshExtent[4];
	void UpdateChairs();
private:
	enum BOUNDS_XY
	{
		MAX_X_MAX_Y,
		MAX_X_MIN_Y,
		MIN_X_MAX_Y,
		MIN_X_MIN_Y
	};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Table, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AActor> TableLegClass;

	void SpawnItem(UClass* ItemToSpawn);

	struct VertexData
	{
		TArray<FVector> Vertices;
		TArray<int32> Triangles;
		TArray<FVector> Normals;
		TArray<FProcMeshTangent> Tangents;
		TArray<FVector2D> UVs; // how to apply the meshes
		TArray<FLinearColor> Colors;
	};

	struct VertexData TableTopMeshVertexData, TableLegMeshVertexData;

	FVector LocalBoundsMax;
	FVector LocalBoundsMin;

	FVector LocalLegBoundsMax;
	FVector LocalLegBoundsMin;

	const int32 NumDraggers = 4;
	TArray<UDragAndDropComponent*> Draggers;
	TMap< UDragAndDropComponent*, BOUNDS_XY> DraggersMap;
	void UpdateDraggersLocation();

	void AddTriangleMesh(FVector Top, FVector BottomLeft, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent);
	void AddQuadMesh(struct VertexData& MeshVertexData, FVector TopLeft, FVector BottomLeft, FVector TopRight, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent);

	void UpdateTableLegLocations();

	// Inherited via IDragAndDropComponent
	virtual void OnChangeAlongX(UDragAndDropComponent* Dragger, float DeltaX) override;
	virtual void OnChangeAlongY(UDragAndDropComponent* Dragger, float DeltaY) override;


	/*
	* Chair spawing members
	*/

	UMyChairPoolComponent* ChairPool;
	TArray<AMyChair*> ChairsInUse;

	FVector ChairMeshOrigin;
	FVector ChairMeshExtent;

	const float MarginChair = 15.0f;
	float LengthChairWithMargins;


	AMyChair* GetChairAt(int32 IndexChair);
	void PlaceChair(AMyChair* Chair, FVector NewLocation, FRotator NewRotation);


	void ScreenLog(FString str)
	{
		if (GEngine)
		{
			FString msg = str;
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, msg);
		}
	}


};
