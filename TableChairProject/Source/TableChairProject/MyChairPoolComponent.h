// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyChairPoolComponent.generated.h"

class AMyChair;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TABLECHAIRPROJECT_API UMyChairPoolComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties

	UMyChairPoolComponent();
	AMyChair* GetReusable();
	
	void AddReusable(AMyChair* Reusable);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	const int32 InitialPoolSize = 20;
	const int32 IncreasePoolSize = 4;
	TArray<AMyChair*> Pool;
		

private:
	bool IncreasePool(int32 IncreasePoolSize);
};
