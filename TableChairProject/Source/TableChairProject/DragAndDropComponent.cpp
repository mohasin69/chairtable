// Fill out your copyright notice in the Description page of Project Settings.


#include "DragAndDropComponent.h"
#include "Engine.h"
#include "IDragAndDropComponent.h"

const FString UDragAndDropComponent::DraggerMeshPath = FString("/Game/Meshes/DraggerCube.DraggerCube");

UDragAndDropComponent::UDragAndDropComponent()
{
    PrimaryComponentTick.bCanEverTick = true;

    bSelected = false;

    DeltaMouseWorldSpaceThreshold = 0.15f;
    DeltaMeshLocationMovement = 20.0f;

    ConstructorHelpers::FObjectFinder<UStaticMesh> DraggerMesh(*DraggerMeshPath);

    if (DraggerMesh.Succeeded())
    {
        UE_LOG(LogTemp, Warning, TEXT("----------------------Succeeded-----------------"));
        SetStaticMesh(DraggerMesh.Object);
        OnClicked.AddDynamic(this, &UDragAndDropComponent::OnClick);
        OnReleased.AddDynamic(this, &UDragAndDropComponent::OnRelease);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("----------------------NOT Succeeded-----------------"));

    }
}

void UDragAndDropComponent::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld())
    {
        MainPlayer = GetWorld()->GetFirstPlayerController();
    }
}


void UDragAndDropComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (bSelected)
    {
        Update();
    }
}

void UDragAndDropComponent::SetMousePositionOverMeshLocation()
{
    if (MainPlayer)
    {
        FVector MouseWorldSpaceLocation;
        FVector MouseWorldSpaceDirection;
        MainPlayer->DeprojectMousePositionToWorld(MouseWorldSpaceLocation, MouseWorldSpaceDirection);

        FVector MeshLocation = GetComponentLocation();
        FVector2D MeshScreenSpaceLocation;
        MainPlayer->ProjectWorldLocationToScreen(MeshLocation, MeshScreenSpaceLocation);
        MainPlayer->SetMouseLocation((int)MeshScreenSpaceLocation.X, (int)MeshScreenSpaceLocation.Y);

        MainPlayer->DeprojectMousePositionToWorld(MouseWorldSpaceLocation, MouseWorldSpaceDirection);
        LastMouseWorldSpaceLocationX = MouseWorldSpaceLocation.X;
        LastMouseWorldSpaceLocationY = MouseWorldSpaceLocation.Y;
    }
}

void UDragAndDropComponent::OnClick(UPrimitiveComponent* pComponent, FKey ButtonPressed)
{
    UE_LOG(LogTemp, Warning, TEXT("----------------------OnClick-----------------"));

    bSelected = true;

    if (MainPlayer)
    {
        FVector MeshLocation = GetComponentLocation();
        FVector2D MeshScreenSpaceLocation;
        MainPlayer->ProjectWorldLocationToScreen(MeshLocation, MeshScreenSpaceLocation);
        MainPlayer->SetMouseLocation((int)MeshScreenSpaceLocation.X, (int)MeshScreenSpaceLocation.Y);

        FVector MouseWorldSpaceLocation;
        FVector MouseWorldSpaceDirection;
        MainPlayer->DeprojectMousePositionToWorld(MouseWorldSpaceLocation, MouseWorldSpaceDirection);

        LastMouseWorldSpaceLocationX = MouseWorldSpaceLocation.X;
        LastMouseWorldSpaceLocationY = MouseWorldSpaceLocation.Y;
    }
}

void UDragAndDropComponent::OnRelease(UPrimitiveComponent* pComponent, FKey ButtonPressed)
{
    UE_LOG(LogTemp, Warning, TEXT("----------------------OnRelease-----------------"));
    bSelected = false;
}

#pragma optimize("", off) 
void UDragAndDropComponent::Update()
{
    if (MainPlayer)
    {
        UE_LOG(LogTemp, Warning, TEXT("----------------------UPDATE-----------------"));
        FVector MouseWorldSpaceLocation;
        FVector MouseWorldSpaceDirection;
        MainPlayer->DeprojectMousePositionToWorld(MouseWorldSpaceLocation, MouseWorldSpaceDirection);

        float DeltaWorldSpaceMouseX = MouseWorldSpaceLocation.X - LastMouseWorldSpaceLocationX;
        float DeltaWorldSpaceMouseY = MouseWorldSpaceLocation.Y - LastMouseWorldSpaceLocationY;

        if (FMath::Abs(DeltaWorldSpaceMouseX) > DeltaMouseWorldSpaceThreshold)
        {
            float DeltaX = FMath::Sign(DeltaWorldSpaceMouseX) * DeltaMeshLocationMovement;
            AddRelativeLocation(FVector(DeltaX, 0.0f, 0.0f));
            NotifyChangeAlongX(DeltaX);
            SetMousePositionOverMeshLocation();
        }
        else if (FMath::Abs(DeltaWorldSpaceMouseY) > DeltaMouseWorldSpaceThreshold)
        {
            float DeltaY = FMath::Sign(DeltaWorldSpaceMouseY) * DeltaMeshLocationMovement;
            AddRelativeLocation(FVector(0.0f, DeltaY, 0.0f));
            NotifyChangeAlongY(DeltaY);
            SetMousePositionOverMeshLocation();
        }
    }


}
#pragma optimize("", on)
void UDragAndDropComponent::SetMeshByReference(UStaticMesh* NewMesh)
{
    if (!!NewMesh)
    {
        SetStaticMesh(NewMesh);
    }
}

void UDragAndDropComponent::SetMeshByPath(const FString& NewMeshPath)
{
    ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(*NewMeshPath);
    if (NewMesh.Succeeded())
    {
        SetStaticMesh(NewMesh.Object);
    }
}

void UDragAndDropComponent::SetMaterialByReference(int32 Index, UMaterial* NewMaterial)
{
    if (!!NewMaterial)
    {
        SetMaterial(Index, NewMaterial);
    }
}

void UDragAndDropComponent::SetMaterialByPath(int32 Index, const FString& NewMaterialPath)
{
    ConstructorHelpers::FObjectFinder<UMaterial> NewMaterial(*NewMaterialPath);
    if (NewMaterial.Succeeded())
    {
        SetMaterial(Index, NewMaterial.Object);
    }
}

void UDragAndDropComponent::NotifyChangeAlongX(float DeltaX)
{
   /* if (GEngine)
    {
        FString msg = "UDragAndDropComponent::NotifyChangeAlongX :: Size " + Observers.Num();
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, msg);
    }*/
    UE_LOG(LogTemp, Warning, TEXT("----------------------UDragAndDropComponent::NotifyChangeAlongX-----------------"));
    for (auto It = Observers.CreateConstIterator(); It; ++It)
    {
        UE_LOG(LogTemp, Warning, TEXT("----------------------UDragAndDropComponent::LOOP X-----------------"));
        (*It)->OnChangeAlongX(this, DeltaX);
    }
}

void UDragAndDropComponent::NotifyChangeAlongY(float DeltaY)
{
   /* if (GEngine)
    {
        FString msg = "UDragAndDropComponent::NotifyChangeAlongY :: Size " + Observers.Num();
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, msg);
    }*/
    UE_LOG(LogTemp, Warning, TEXT("----------------------UDragAndDropComponent::NotifyChangeAlongY-----------------"));
    for (auto It = Observers.CreateConstIterator(); It; ++It)
    {
        UE_LOG(LogTemp, Warning, TEXT("----------------------UDragAndDropComponent::LOOP Y-----------------"));
        (*It)->OnChangeAlongY(this, DeltaY);
    }
}