// Fill out your copyright notice in the Description page of Project Settings.


#include "MyChair.h"

// Sets default values
FVector AMyChair::ChairMeshExtent;
AMyChair::AMyChair()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	LocalBoundsMax = FVector(
		(ChairBaseSizeLowerLimitX / 2),
		(ChairBaseSizeLowerLimitY / 2),
		(ChairBaseSizeLowerLimitZ / 2));

	LocalBoundsMin = FVector(
		-(ChairBaseSizeLowerLimitX / 2),
		-(ChairBaseSizeLowerLimitY / 2),
		-(ChairBaseSizeLowerLimitZ / 2));

	LocalLegBoundsMax = FVector(
		(ChairLegSizeLowerLimitX / 2),
		(ChairLegSizeLowerLimitY / 2),
		(ChairLegSizeLowerLimitZ / 2));

	LocalLegBoundsMin = FVector(
		-(ChairLegSizeLowerLimitX / 2),
		-(ChairLegSizeLowerLimitY / 2),
		-(ChairLegSizeLowerLimitZ / 2));


	LocalChairBackBoundsMax = FVector(
		(ChairBackSizeLowerLimitX / 2),
		(ChairBackSizeLowerLimitY / 2),
		(ChairBackSizeLowerLimitZ / 2));

	LocalChairBackBoundsMin = FVector(
		-(ChairBackSizeLowerLimitX / 2),
		-(ChairBackSizeLowerLimitY / 2),
		-(ChairBackSizeLowerLimitZ / 2));


	ThisChairScene = CreateDefaultSubobject<USceneComponent>(TEXT("RootChair"));
	RootComponent = ThisChairScene;

	ThisChairMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GenerateChairMesh"));
	ThisChairMesh->SetupAttachment(RootComponent);

	for (int i = 0; i < NumLegs; ++i)
	{
		FName name = "ChairLeg_" + i;
		ThisChairLegMesh[i] = CreateDefaultSubobject<UProceduralMeshComponent>(name);
		ThisChairLegMesh[i]->SetupAttachment(ThisChairMesh);
	}
	
	FName name = "ChairBackMesh";
	ThisChairBackMesh = CreateDefaultSubobject<UProceduralMeshComponent>(name);
	ThisChairBackMesh->SetupAttachment(ThisChairMesh);

	ChairMeshExtent = ThisChairMesh->GetLocalBounds().BoxExtent;

	GenerateChairBase();
}

void AMyChair::GenerateChairBase()
{
	ChairBaseMeshVertexData.
		Vertices = {
			FVector(LocalBoundsMin.X,  LocalBoundsMax.Y,  ChairBaseHeight + LocalBoundsMax.Z),
			FVector(LocalBoundsMin.X,  LocalBoundsMin.Y,  ChairBaseHeight + LocalBoundsMax.Z),
			FVector(LocalBoundsMin.X,  LocalBoundsMax.Y,  ChairBaseHeight + LocalBoundsMin.Z),
			FVector(LocalBoundsMin.X,  LocalBoundsMin.Y,  ChairBaseHeight + LocalBoundsMin.Z),
			FVector(LocalBoundsMax.X,  LocalBoundsMax.Y,  ChairBaseHeight + LocalBoundsMax.Z),
			FVector(LocalBoundsMax.X,  LocalBoundsMin.Y,  ChairBaseHeight + LocalBoundsMax.Z),
			FVector(LocalBoundsMax.X,  LocalBoundsMax.Y,  ChairBaseHeight + LocalBoundsMin.Z),
			FVector(LocalBoundsMax.X,  LocalBoundsMin.Y,  ChairBaseHeight + LocalBoundsMin.Z)
	};

	ChairBaseMeshVertexData.
		Normals = {
			FVector(-1.0f,  1.0f,  1.0f),
			FVector(-1.0f, -1.0f,  1.0f),
			FVector(-1.0f,  1.0f, -1.0f),
			FVector(-1.0f, -1.0f, -1.0f),
			FVector(1.0f,  1.0f,  1.0f),
			FVector(1.0f, -1.0f,  1.0f),
			FVector(1.0f,  1.0f, -1.0f),
			FVector(1.0f, -1.0f, -1.0f)
	};

	ChairBaseMeshVertexData.
		Triangles = {
			0, 1, 2,
			2, 1, 3,
			4, 5, 0,
			0, 5, 1,
			6, 7, 4,
			4, 7, 5,
			2, 7, 6,
			3, 7, 2,
			4, 0, 6,
			6, 0, 2,
			1, 5, 3,
			3, 5, 7,
	};

	ThisChairMesh->CreateMeshSection_LinearColor(
		0,
		ChairBaseMeshVertexData.Vertices,
		ChairBaseMeshVertexData.Triangles,
		ChairBaseMeshVertexData.Normals,
		ChairBaseMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		false
	);

	GenerateChairLegs();
	GenerateChairBack();
}

void AMyChair::GenerateChairBack()
{
	ChairBackMeshVertexData.
		Vertices = {
			FVector(LocalChairBackBoundsMin.X,  LocalChairBackBoundsMax.Y,  (ChairBaseHeight) + LocalChairBackBoundsMax.Z),
			FVector(LocalChairBackBoundsMin.X,  LocalChairBackBoundsMin.Y,  (ChairBaseHeight) + LocalChairBackBoundsMax.Z),
			FVector(LocalChairBackBoundsMin.X,  LocalChairBackBoundsMax.Y,  (ChairBaseHeight) + LocalChairBackBoundsMin.Z),
			FVector(LocalChairBackBoundsMin.X,  LocalChairBackBoundsMin.Y,  (ChairBaseHeight) + LocalChairBackBoundsMin.Z),
			FVector(LocalChairBackBoundsMax.X,  LocalChairBackBoundsMax.Y,  (ChairBaseHeight) + LocalChairBackBoundsMax.Z),
			FVector(LocalChairBackBoundsMax.X,  LocalChairBackBoundsMin.Y,  (ChairBaseHeight) + LocalChairBackBoundsMax.Z),
			FVector(LocalChairBackBoundsMax.X,  LocalChairBackBoundsMax.Y,  (ChairBaseHeight) + LocalChairBackBoundsMin.Z),
			FVector(LocalChairBackBoundsMax.X,  LocalChairBackBoundsMin.Y,  (ChairBaseHeight) + LocalChairBackBoundsMin.Z)
	};

	ChairBackMeshVertexData.
		Normals = {
			FVector(-1.0f,  1.0f,  1.0f),
			FVector(-1.0f, -1.0f,  1.0f),
			FVector(-1.0f,  1.0f, -1.0f),
			FVector(-1.0f, -1.0f, -1.0f),
			FVector(1.0f,  1.0f,  1.0f),
			FVector(1.0f, -1.0f,  1.0f),
			FVector(1.0f,  1.0f, -1.0f),
			FVector(1.0f, -1.0f, -1.0f)
	};

	ChairBackMeshVertexData.
		Triangles = {
			0, 1, 2,
			2, 1, 3,
			4, 5, 0,
			0, 5, 1,
			6, 7, 4,
			4, 7, 5,
			2, 7, 6,
			3, 7, 2,
			4, 0, 6,
			6, 0, 2,
			1, 5, 3,
			3, 5, 7,
	};

	ThisChairBackMesh->CreateMeshSection_LinearColor(
		0,
		ChairBackMeshVertexData.Vertices,
		ChairBackMeshVertexData.Triangles,
		ChairBackMeshVertexData.Normals,
		ChairBackMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		false
	);

	ChairBackMeshExtent = ThisChairBackMesh->GetLocalBounds().BoxExtent;

	ThisChairBackMesh->SetRelativeLocation(FVector(LocalBoundsMin.X + ChairBackMeshExtent.X,
		LocalBoundsMax.Y - ChairBackMeshExtent.Y,
		ChairBaseHeight*1.20 + LocalBoundsMin.Z - ChairBackMeshExtent.Z));
	
}

void AMyChair::GenerateChairLegs()
{

	ChairLegMeshVertexData.ResetAll();

	int32 TriangleIndexCount = 0; // Keep track of triangles

	FVector DefinedShape[8];
	FProcMeshTangent TangentSetup;

	ChairLegMeshVertexData.Vertices = {
							   FVector(LocalLegBoundsMin.X,  LocalLegBoundsMax.Y,  LocalLegBoundsMax.Z),
							   FVector(LocalLegBoundsMin.X,  LocalLegBoundsMin.Y,  LocalLegBoundsMax.Z),
							   FVector(LocalLegBoundsMin.X,  LocalLegBoundsMax.Y,  LocalLegBoundsMin.Z),
							   FVector(LocalLegBoundsMin.X,  LocalLegBoundsMin.Y,  LocalLegBoundsMin.Z),
							   FVector(LocalLegBoundsMax.X,  LocalLegBoundsMax.Y,  LocalLegBoundsMax.Z),
							   FVector(LocalLegBoundsMax.X,  LocalLegBoundsMin.Y,  LocalLegBoundsMax.Z),
							   FVector(LocalLegBoundsMax.X,  LocalLegBoundsMax.Y,  LocalLegBoundsMin.Z),
							   FVector(LocalLegBoundsMax.X,  LocalLegBoundsMin.Y,  LocalLegBoundsMin.Z) };

	ChairLegMeshVertexData.Triangles = { 0, 1, 2,
					   2, 1, 3,
					   4, 5, 0,
					   0, 5, 1,
					   6, 7, 4,
					   4, 7, 5,
					   2, 7, 6,
					   3, 7, 2,
					   4, 0, 6,
					   6, 0, 2,
					   1, 5, 3,
					   3, 5, 7, };

	ChairLegMeshVertexData.Normals = { FVector(-1.0f,  1.0f,  1.0f),
							   FVector(-1.0f, -1.0f,  1.0f),
							   FVector(-1.0f,  1.0f, -1.0f),
							   FVector(-1.0f, -1.0f, -1.0f),
							   FVector(1.0f,  1.0f,  1.0f),
							   FVector(1.0f, -1.0f,  1.0f),
							   FVector(1.0f,  1.0f, -1.0f),
							   FVector(1.0f, -1.0f, -1.0f) };



	float ZCoord = 0;

	FVector LegLocations[] = { FVector(LocalBoundsMax.X - ChairLegMeshExtent[0].X,
										LocalBoundsMax.Y - ChairLegMeshExtent[0].Y,
										ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[0].Z),
								FVector(LocalBoundsMax.X - ChairLegMeshExtent[1].X,
										LocalBoundsMin.Y + ChairLegMeshExtent[1].Y,
										ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[1].Z),
								FVector(LocalBoundsMin.X + ChairLegMeshExtent[2].X,
										LocalBoundsMax.Y - ChairLegMeshExtent[2].Y,
										ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[2].Z),
								FVector(LocalBoundsMin.X + ChairLegMeshExtent[3].X,
										LocalBoundsMin.Y + ChairLegMeshExtent[3].Y,
										ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[3].Z) };


	ThisChairLegMesh[0]->CreateMeshSection_LinearColor(0,
		ChairLegMeshVertexData.Vertices,
		ChairLegMeshVertexData.Triangles,
		ChairLegMeshVertexData.Normals,
		ChairLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	ChairLegMeshExtent[0] = ThisChairLegMesh[0]->GetLocalBounds().BoxExtent;

	ThisChairLegMesh[0]->SetRelativeLocation(FVector(LocalBoundsMax.X - ChairLegMeshExtent[0].X,
		LocalBoundsMax.Y - ChairLegMeshExtent[0].Y,
		ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[0].Z));


	ThisChairLegMesh[1]->CreateMeshSection_LinearColor(0,
		ChairLegMeshVertexData.Vertices,
		ChairLegMeshVertexData.Triangles,
		ChairLegMeshVertexData.Normals,
		ChairLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	ChairLegMeshExtent[1] = ThisChairLegMesh[1]->GetLocalBounds().BoxExtent;

	ThisChairLegMesh[1]->SetRelativeLocation(FVector(LocalBoundsMax.X - ChairLegMeshExtent[1].X,
		LocalBoundsMin.Y + ChairLegMeshExtent[1].Y,
		ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[1].Z));


	ThisChairLegMesh[2]->CreateMeshSection_LinearColor(0,
		ChairLegMeshVertexData.Vertices,
		ChairLegMeshVertexData.Triangles,
		ChairLegMeshVertexData.Normals,
		ChairLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	ChairLegMeshExtent[2] = ThisChairLegMesh[2]->GetLocalBounds().BoxExtent;

	ThisChairLegMesh[2]->SetRelativeLocation(FVector(LocalBoundsMin.X + ChairLegMeshExtent[2].X,
		LocalBoundsMax.Y - ChairLegMeshExtent[2].Y,
		ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[2].Z));

	ThisChairLegMesh[3]->CreateMeshSection_LinearColor(0,
		ChairLegMeshVertexData.Vertices,
		ChairLegMeshVertexData.Triangles,
		ChairLegMeshVertexData.Normals,
		ChairLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	ChairLegMeshExtent[3] = ThisChairLegMesh[3]->GetLocalBounds().BoxExtent;

	ThisChairLegMesh[3]->SetRelativeLocation(FVector(LocalBoundsMin.X + ChairLegMeshExtent[3].X,
		LocalBoundsMin.Y + ChairLegMeshExtent[3].Y,
		ChairBaseHeight + LocalBoundsMin.Z - ChairLegMeshExtent[3].Z));



}



void AMyChair::AddQuadMesh(VertexData& MeshVertexData, FVector TopLeft, FVector BottomLeft, FVector TopRight, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;
	int32 Point4 = TriIndex++;

	MeshVertexData.Vertices.Add(TopRight);
	MeshVertexData.Vertices.Add(BottomRight);
	MeshVertexData.Vertices.Add(TopLeft);
	MeshVertexData.Vertices.Add(BottomLeft);

	MeshVertexData.Triangles.Add(Point1);
	MeshVertexData.Triangles.Add(Point2);
	MeshVertexData.Triangles.Add(Point3);


	MeshVertexData.Triangles.Add(Point4);
	MeshVertexData.Triangles.Add(Point3);
	MeshVertexData.Triangles.Add(Point2);

	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, TopLeft - TopRight).GetSafeNormal();

	for (int i = 0; i < 4; ++i)
	{
		//MeshVertexData.Normals.Add(ThisNorm);
		//MeshVertexData.Tangents.Add(Tangent);
		MeshVertexData.Colors.Add(FLinearColor::Green);
	}

	MeshVertexData.UVs.Add(FVector2D(0.f, 1.f)); // Top left
	MeshVertexData.UVs.Add(FVector2D(0.f, 0.f)); // Bottom Left
	MeshVertexData.UVs.Add(FVector2D(1.f, 1.f)); // Top Right
	MeshVertexData.UVs.Add(FVector2D(1.f, 0.f)); // Bottom Right
}


void AMyChair::Enable()
{
	SetActorHiddenInGame(false);
	//SetVisibility(true);
}

void AMyChair::Disable()
{
	SetActorHiddenInGame(true);
	//SetVisibility(false);
}


void AMyChair::PostActorCreated()
{
}

void AMyChair::PostLoad()
{
	Super::PostLoad();

}

//
//FVector AMyChair::GetChairMeshOrigin()
//{
//	return AMyChair::ChairMeshExtent;
//}
//
//FVector AMyChair::GetChairMeshExtent()
//{
//	return AMyChair::ChairMeshExtent;
//
//}
