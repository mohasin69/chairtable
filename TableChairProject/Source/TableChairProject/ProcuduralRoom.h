// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TableBase.h"
#include "ProcuduralRoom.generated.h"

class AMyChair;
class UMyChairPoolComponent;


UCLASS()
class TABLECHAIRPROJECT_API AProcuduralRoom : public ATableBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProcuduralRoom();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Update() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* Floor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AActor> ChairClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Room, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AActor> TableClass;

	void SpawnItem(UClass*);

};
