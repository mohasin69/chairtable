// Fill out your copyright notice in the Description page of Project Settings.


#include "MyChairPoolComponent.h"
#include "MyChair.h"

// Sets default values for this component's properties
UMyChairPoolComponent::UMyChairPoolComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


AMyChair* UMyChairPoolComponent::GetReusable()
{
    if (!Pool.Num())
    {
        if (!IncreasePool(IncreasePoolSize))
        {
            return nullptr;
        }
    }

    AMyChair* Reusable = Pool.Pop(false);
    Reusable->Enable();
    return Reusable;
}

void UMyChairPoolComponent::AddReusable(AMyChair* Reusable)
{
    Reusable->Disable();
    Pool.Emplace(Reusable);
}

void UMyChairPoolComponent::BeginPlay()
{
    Super::BeginPlay();
    IncreasePool(InitialPoolSize);
}

bool UMyChairPoolComponent::IncreasePool(int32 IncreasePoolSizeInput)
{
    int32 CurrentPoolSize = Pool.GetSlack();

    Pool.Reset(CurrentPoolSize + IncreasePoolSizeInput);

    int32 NewPoolSize = Pool.GetSlack();

    if (Pool.GetSlack() == CurrentPoolSize)
    {
        return false;
    }

    for (int32 i = CurrentPoolSize; i < NewPoolSize; i++)
    {
        FString ChairComponentName = "ChairComponent_" + FString::FromInt(i);
        AMyChair* NewChairComponent = NewObject<AMyChair>(this, FName(*ChairComponentName));
        NewChairComponent->RegisterAllComponents();
        NewChairComponent->Disable();

        Pool.Emplace(NewChairComponent);
    }

    return true;
}



//// Called every frame
//void UMyChairPoolComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	// ...
//}
//
