// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MyChair.generated.h"

UCLASS()
class TABLECHAIRPROJECT_API AMyChair : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyChair();

protected:
	// Called when the game starts or when spawned
	void GenerateChairBase();
	void GenerateChairBack();
	void GenerateChairLegs();

	UPROPERTY(VisibleAnywhere)
		USceneComponent* ThisChairScene;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisChairMesh;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisChairLegMesh[4];

	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* ThisChairBackMesh;

	virtual void PostActorCreated() override;
	virtual void PostLoad() override;

	FVector ChairLegMeshExtent[4];
	FVector ChairBackMeshExtent;


public:	
	// Called every frame
	//virtual void Tick(float DeltaTime) override;
	//static FVector GetChairMeshOrigin();
	//static FVector GetChairMeshExtent();
	static FVector ChairMeshExtent;
	void Enable();
	void Disable();

private:
	const int32 NumLegs = 4;
	const float ChairBaseSizeLowerLimitX = 30.0f;
	const float ChairBaseSizeLowerLimitY = 30.0f;
	const float ChairBaseSizeLowerLimitZ = 10.0f;

	const float ChairBaseHeight = 32.5f;

	const float ChairLegSizeLowerLimitX = 10.0f;
	const float ChairLegSizeLowerLimitY = 10.0f;
	const float ChairLegSizeLowerLimitZ = ChairBaseHeight;

	const float ChairBackSizeLowerLimitX = 5.0f;
	const float ChairBackSizeLowerLimitY = 30.0f;
	const float ChairBackSizeLowerLimitZ = 30.0f;

	FVector LocalBoundsMax;
	FVector LocalBoundsMin;

	FVector LocalLegBoundsMax;
	FVector LocalLegBoundsMin;

	FVector LocalChairBackBoundsMax;
	FVector LocalChairBackBoundsMin;

	struct VertexData
	{
		TArray<FVector> Vertices;
		TArray<int32> Triangles;
		TArray<FVector> Normals;
		TArray<FProcMeshTangent> Tangents;
		TArray<FVector2D> UVs;
		TArray<FLinearColor> Colors;
		void ResetAll()
		{
			Vertices.Reset();
			Triangles.Reset();
			Normals.Reset();
			Tangents.Reset();
			UVs.Reset();
			Colors.Reset();
		}
	};
	struct VertexData ChairBaseMeshVertexData, ChairBackMeshVertexData, ChairLegMeshVertexData;

	typedef std::unordered_map<UProceduralMeshComponent*, FVector> CHAIR_LEGS_MAP;
	std::vector<CHAIR_LEGS_MAP> ChairLegsPoolList;



	uint32 ChairBaseCount = 0;

	void AddQuadMesh(struct VertexData& MeshVertexData, FVector TopLeft, FVector BottomLeft, FVector TopRight, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent);

};
