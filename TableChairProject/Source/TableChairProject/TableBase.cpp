// Fill out your copyright notice in the Description page of Project Settings.


#include "TableBase.h"
#include "MyChairPoolComponent.h"
#include "MyChair.h"

// Sets default values
ATableBase::ATableBase()
{
	PrimaryActorTick.bCanEverTick = false;
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	LocalBoundsMax = FVector(
		(TableSizeLowerLimitX / 2),
		(TableSizeLowerLimitY / 2),
		(TableSizeLowerLimitZ / 2));

	LocalBoundsMin = FVector(
		-(TableSizeLowerLimitX / 2),
		-(TableSizeLowerLimitY / 2),
		-(TableSizeLowerLimitZ / 2));

	LocalLegBoundsMax = FVector(
		(TableLegSizeLowerLimitX / 2),
		(TableLegSizeLowerLimitY / 2),
		(TableLegSizeLowerLimitZ / 2));

	LocalLegBoundsMin = FVector(
		-(TableLegSizeLowerLimitX / 2),
		-(TableLegSizeLowerLimitY / 2),
		-(TableLegSizeLowerLimitZ / 2));


	ThisScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisScene;

	ThisTableMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Generatemesh"));
	ThisTableMesh->SetupAttachment(RootComponent);


	for (int i = 0; i < NumLegs; ++i)
	{
		FName name = "TableLeg_" + i;
		ThisTableLegMesh[i] = CreateDefaultSubobject<UProceduralMeshComponent>(name);
		ThisTableLegMesh[i]->SetupAttachment(ThisTableMesh);
	}

	/*
	* Chair Members
	*/
	

	//ThisTableLegMesh->CreateMeshSection_LinearColor
	Generatemesh();

	Draggers.Reset(NumDraggers);
	for (int32 i = 0; i < NumDraggers; i++)
	{
		FString DraggerName = "Dragger_" + FString::FromInt(i);
		UDragAndDropComponent* NewDragAndDropComponent = CreateDefaultSubobject<UDragAndDropComponent>(FName(*DraggerName));
		NewDragAndDropComponent->SetupAttachment(RootComponent);

		DraggersMap.Emplace(NewDragAndDropComponent, (BOUNDS_XY)i);
		Draggers.Emplace(NewDragAndDropComponent);
	}
	UpdateDraggersLocation();

	
	ChairPool = CreateDefaultSubobject<UMyChairPoolComponent>(TEXT("ChairPool1"));
	ChairMeshExtent = AMyChair::ChairMeshExtent;

	LengthChairWithMargins = (ChairMeshExtent.Y * 2) + (MarginChair * 2);

}


void ATableBase::Update()
{
	UpdateDraggersLocation();
	UpdateChairs();
}

void ATableBase::UpdateDraggersLocation()
{
	FVector TableBoundsMax = GetLocalBoundsMax();
	FVector TableBoundsMin = GetLocalBoundsMin();

	float ZCoord = TableHeight;

	FVector DraggerLocations[] = { FVector(TableBoundsMax.X, 
											TableBoundsMax.Y,
											ZCoord),
									FVector(TableBoundsMax.X,
											TableBoundsMin.Y,
											ZCoord),
									FVector(TableBoundsMin.X,
											TableBoundsMax.Y,
											ZCoord),
									FVector(TableBoundsMin.X,
											TableBoundsMin.Y,
											ZCoord) };


	if (Draggers.Num() == NumDraggers)
	{
		for (int i = 0; i < NumDraggers; i++)
		{
			Draggers[i]->SetRelativeLocation(DraggerLocations[i]);
			Draggers[i]->AddObserver(this);
		}
	}
}


void ATableBase::UpdateLocalBoundsMax(FVector NewLocalBoundsMax)
{
	if ((NewLocalBoundsMax.X - LocalBoundsMin.X) < TableSizeLowerLimitX)
	{
		NewLocalBoundsMax.X = LocalBoundsMin.X + TableSizeLowerLimitX;
	}
	if ((NewLocalBoundsMax.Y - LocalBoundsMin.Y) < TableSizeLowerLimitY)
	{
		NewLocalBoundsMax.Y = LocalBoundsMin.Y + TableSizeLowerLimitY;
	}
	LocalBoundsMax = NewLocalBoundsMax;

	UpdateChairs();
	UpdateTableTopMesh();
	UpdateTableLegLocations();
	UpdateDraggersLocation();
}

void ATableBase::UpdateLocalBoundsMin(FVector NewLocalBoundsMin)
{
	if ((LocalBoundsMax.X - NewLocalBoundsMin.X) < TableSizeLowerLimitX)
	{
		NewLocalBoundsMin.X = LocalBoundsMax.X - TableSizeLowerLimitX;
	}
	if ((LocalBoundsMax.Y - NewLocalBoundsMin.Y) < TableSizeLowerLimitY)
	{
		NewLocalBoundsMin.Y = LocalBoundsMax.Y - TableSizeLowerLimitY;
	}
	LocalBoundsMin = NewLocalBoundsMin;

	UpdateChairs();
	UpdateTableTopMesh();
	UpdateTableLegLocations();
	UpdateDraggersLocation();
}


void ATableBase::PostActorCreated()
{
	Super::PostActorCreated();
}

void ATableBase::PostLoad()
{
	Super::PostLoad();

}



void ATableBase::Generatemesh(bool updateTheMesh)
{
	TableTopMeshVertexData.Vertices.Reset();
	TableTopMeshVertexData.Triangles.Reset();
	TableTopMeshVertexData.Normals.Reset();
	TableTopMeshVertexData.Tangents.Reset();
	TableTopMeshVertexData.UVs.Reset();
	TableTopMeshVertexData.Colors.Reset();

	int32 TriangleIndexCount = 0; // Keep track of triangles

	FVector DefinedShape[8];
	FProcMeshTangent TangentSetup;

	DefinedShape[0] = FVector(LocalBoundsMax.X, LocalBoundsMax.Y, TableHeight + LocalBoundsMax.Z);
	DefinedShape[1] = FVector(LocalBoundsMax.X, LocalBoundsMax.Y, TableHeight + LocalBoundsMin.Z);
	DefinedShape[2] = FVector(LocalBoundsMax.X, LocalBoundsMin.Y, TableHeight + LocalBoundsMax.Z);
	DefinedShape[3] = FVector(LocalBoundsMax.X, LocalBoundsMin.Y, TableHeight + LocalBoundsMin.Z);

	DefinedShape[4] = FVector(LocalBoundsMin.X, LocalBoundsMin.Y, TableHeight + LocalBoundsMax.Z);
	DefinedShape[5] = FVector(LocalBoundsMin.X, LocalBoundsMin.Y, TableHeight + LocalBoundsMin.Z);
	DefinedShape[6] = FVector(LocalBoundsMin.X, LocalBoundsMax.Y, TableHeight + LocalBoundsMax.Z);
	DefinedShape[7] = FVector(LocalBoundsMin.X, LocalBoundsMax.Y, TableHeight + LocalBoundsMin.Z);


	//DefinedShape[0] = FVector(CubeRadius.X, CubeRadius.Y, (TableHeight) +(CubeRadius.Z)); //  top right
	//DefinedShape[1] = FVector(CubeRadius.X, CubeRadius.Y, (TableHeight)+(-CubeRadius.Z)); //  bottom right
	//DefinedShape[2] = FVector(CubeRadius.X, -CubeRadius.Y, (TableHeight)+(CubeRadius.Z)); //  top left
	//DefinedShape[3] = FVector(CubeRadius.X, -CubeRadius.Y, (TableHeight)+(-CubeRadius.Z)); //  bottom left
	//DefinedShape[4] = FVector(-CubeRadius.X, -CubeRadius.Y, (TableHeight)+(CubeRadius.Z)); //  bottom right
	//DefinedShape[5] = FVector(-CubeRadius.X, -CubeRadius.Y, (TableHeight)+(-CubeRadius.Z)); //  top right
	//DefinedShape[6] = FVector(-CubeRadius.X, CubeRadius.Y, (TableHeight)+(CubeRadius.Z)); //  bottom left
	//DefinedShape[7] = FVector(-CubeRadius.X, CubeRadius.Y, (TableHeight)+(-CubeRadius.Z)); //  top left

	//FRONT
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[0], DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);

	//LEFT
	TangentSetup = FProcMeshTangent(1.f, 0.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[2], DefinedShape[3], DefinedShape[4], DefinedShape[5], TriangleIndexCount, TangentSetup);

	//BACK
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[4], DefinedShape[5], DefinedShape[6], DefinedShape[7], TriangleIndexCount, TangentSetup);

	//RIGHT
	TangentSetup = FProcMeshTangent(-1.f, -1.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[6], DefinedShape[7], DefinedShape[0], DefinedShape[1], TriangleIndexCount, TangentSetup);

	//TOP
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[6], DefinedShape[0], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);

	//DOWN
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(TableTopMeshVertexData, DefinedShape[1], DefinedShape[7], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);

	if (updateTheMesh)
	{
		ThisTableMesh->UpdateMeshSection_LinearColor(
			0,
			TableTopMeshVertexData.Vertices,
			TArray<FVector>(),
			TArray<FVector2D>(),
			TArray<FLinearColor>(),
			TArray<FProcMeshTangent>()
		);
	}
	else
	{
		ThisTableMesh->CreateMeshSection_LinearColor(0,
			TableTopMeshVertexData.Vertices,
			TableTopMeshVertexData.Triangles,
			TableTopMeshVertexData.Normals,
			TableTopMeshVertexData.UVs,
			TArray<FLinearColor>(),
			TArray<FProcMeshTangent>(),
			true);
	}
	//ThisTableMesh->CreateMeshSection_LinearColor(0, TableTopMeshVertexData.Vertices, TableTopMeshVertexData.Triangles, TArray<FVector>(), TArray<FVector2D>(), TArray<FLinearColor>(), TArray<FProcMeshTangent>(), true);
	GenerateTableLeg();
	UpdateChairs();
}


void ATableBase::GenerateTableLeg()
{
	TableLegMeshVertexData.Vertices.Reset();
	TableLegMeshVertexData.Triangles.Reset();
	TableLegMeshVertexData.Normals.Reset();
	TableLegMeshVertexData.Tangents.Reset();
	TableLegMeshVertexData.UVs.Reset();
	TableLegMeshVertexData.Colors.Reset();

	int32 TriangleIndexCount = 0; // Keep track of triangles

	FVector DefinedShape[8];
	FProcMeshTangent TangentSetup;

	DefinedShape[0] = FVector(LocalLegBoundsMax.X, LocalLegBoundsMax.Y, LocalLegBoundsMax.Z);
	DefinedShape[1] = FVector(LocalLegBoundsMax.X, LocalLegBoundsMax.Y, LocalLegBoundsMin.Z);
	DefinedShape[2] = FVector(LocalLegBoundsMax.X, LocalLegBoundsMin.Y, LocalLegBoundsMax.Z);
	DefinedShape[3] = FVector(LocalLegBoundsMax.X, LocalLegBoundsMin.Y, LocalLegBoundsMin.Z);

	DefinedShape[4] = FVector(LocalLegBoundsMin.X, LocalLegBoundsMin.Y, LocalLegBoundsMax.Z);
	DefinedShape[5] = FVector(LocalLegBoundsMin.X, LocalLegBoundsMin.Y, LocalLegBoundsMin.Z);
	DefinedShape[6] = FVector(LocalLegBoundsMin.X, LocalLegBoundsMax.Y, LocalLegBoundsMax.Z);
	DefinedShape[7] = FVector(LocalLegBoundsMin.X, LocalLegBoundsMax.Y, LocalLegBoundsMin.Z);

	//FRONT
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[0], DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);

	//LEFT
	TangentSetup = FProcMeshTangent(1.f, 0.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[2], DefinedShape[3], DefinedShape[4], DefinedShape[5], TriangleIndexCount, TangentSetup);

	//BACK
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[4], DefinedShape[5], DefinedShape[6], DefinedShape[7], TriangleIndexCount, TangentSetup);

	//RIGHT
	TangentSetup = FProcMeshTangent(-1.f, -1.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[6], DefinedShape[7], DefinedShape[0], DefinedShape[1], TriangleIndexCount, TangentSetup);

	//TOP
	TangentSetup = FProcMeshTangent(0.f, 1.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[6], DefinedShape[0], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);

	//DOWN
	TangentSetup = FProcMeshTangent(0.f, -1.f, 0.f);
	AddQuadMesh(TableLegMeshVertexData, DefinedShape[1], DefinedShape[7], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);




	float ZCoord = 0;

	FVector LegLocations[] = { FVector(LocalBoundsMax.X - TableLegMeshExtent[0].X,
										LocalBoundsMax.Y - TableLegMeshExtent[0].Y,
										TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[0].Z),
								FVector(LocalBoundsMax.X - TableLegMeshExtent[1].X,
										LocalBoundsMin.Y + TableLegMeshExtent[1].Y,
										TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[1].Z),
								FVector(LocalBoundsMin.X + TableLegMeshExtent[2].X,
										LocalBoundsMax.Y - TableLegMeshExtent[2].Y,
										TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[2].Z),
								FVector(LocalBoundsMin.X + TableLegMeshExtent[3].X,
										LocalBoundsMin.Y + TableLegMeshExtent[3].Y,
										TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[3].Z) };


	ThisTableLegMesh[0]->CreateMeshSection_LinearColor(0,
		TableLegMeshVertexData.Vertices,
		TableLegMeshVertexData.Triangles,
		TableLegMeshVertexData.Normals,
		TableLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	TableLegMeshExtent[0] = ThisTableLegMesh[0]->GetLocalBounds().BoxExtent;

	ThisTableLegMesh[0]->SetRelativeLocation(FVector(LocalBoundsMax.X - TableLegMeshExtent[0].X,
		LocalBoundsMax.Y - TableLegMeshExtent[0].Y,
		TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[0].Z));


	ThisTableLegMesh[1]->CreateMeshSection_LinearColor(0,
		TableLegMeshVertexData.Vertices,
		TableLegMeshVertexData.Triangles,
		TableLegMeshVertexData.Normals,
		TableLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	TableLegMeshExtent[1] = ThisTableLegMesh[1]->GetLocalBounds().BoxExtent;

	ThisTableLegMesh[1]->SetRelativeLocation(FVector(LocalBoundsMax.X - TableLegMeshExtent[1].X,
		LocalBoundsMin.Y + TableLegMeshExtent[1].Y,
		TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[1].Z));


	ThisTableLegMesh[2]->CreateMeshSection_LinearColor(0,
		TableLegMeshVertexData.Vertices,
		TableLegMeshVertexData.Triangles,
		TableLegMeshVertexData.Normals,
		TableLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	TableLegMeshExtent[2] = ThisTableLegMesh[2]->GetLocalBounds().BoxExtent;

	ThisTableLegMesh[2]->SetRelativeLocation(FVector(LocalBoundsMin.X + TableLegMeshExtent[2].X,
		LocalBoundsMax.Y - TableLegMeshExtent[2].Y,
		TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[2].Z));

	ThisTableLegMesh[3]->CreateMeshSection_LinearColor(0,
		TableLegMeshVertexData.Vertices,
		TableLegMeshVertexData.Triangles,
		TableLegMeshVertexData.Normals,
		TableLegMeshVertexData.UVs,
		TArray<FLinearColor>(),
		TArray<FProcMeshTangent>(),
		true);
	TableLegMeshExtent[3] = ThisTableLegMesh[3]->GetLocalBounds().BoxExtent;

	ThisTableLegMesh[3]->SetRelativeLocation(FVector(LocalBoundsMin.X + TableLegMeshExtent[3].X,
		LocalBoundsMin.Y + TableLegMeshExtent[3].Y,
		TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[3].Z));


}

void ATableBase::SpawnItem(UClass* ItemToSpawn)
{
	/*float ZCoord = TableHeight + LocalBoundsMin.Z - TableLegMeshExtent.Z;
	GetWorld()->SpawnActor<AActor>(ItemToSpawn,
		FVector(LocalBoundsMax.X - TableLegMeshExtent.X, LocalBoundsMax.Y - TableLegMeshExtent.Y, ZCoord),
		FRotator(0.f)
		);*/

	//GetWorld()->SpawnActor<AActor>(ChairClass, FVector(0.f), FRotator(0.f));

}

void ATableBase::AddTriangleMesh(FVector Top, FVector BottomLeft, FVector BottomRight, int32& TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	TableTopMeshVertexData.Vertices.Add(Top);
	TableTopMeshVertexData.Vertices.Add(BottomLeft);
	TableTopMeshVertexData.Vertices.Add(BottomRight);

	TableTopMeshVertexData.Triangles.Add(Point1);
	TableTopMeshVertexData.Triangles.Add(Point2);
	TableTopMeshVertexData.Triangles.Add(Point3);

	FVector ThisNorm = FVector::CrossProduct(Top, BottomRight).GetSafeNormal();

	for (int i = 0; i < 3; ++i)
	{
		TableTopMeshVertexData.Normals.Add(ThisNorm);
		TableTopMeshVertexData.Tangents.Add(Tangent);
		TableTopMeshVertexData.Colors.Add(FLinearColor::Green);
	}

	TableTopMeshVertexData.UVs.Add(FVector2D(0.f, 1.f));
	TableTopMeshVertexData.UVs.Add(FVector2D(0.f, 0.f));
	TableTopMeshVertexData.UVs.Add(FVector2D(1.f, 0.f));
}


void ATableBase::UpdateTableTopMesh()
{
	Generatemesh(true);
}

void ATableBase::UpdateTableLegLocations()
{
	UE_LOG(LogTemp, Warning, TEXT("----------------------ATableBase::UpdateTableLegLocations-----------------"));
	float ZCoord = TableHeight + LocalBoundsMin.Z - TableLegMeshExtent[0].Z;

	FVector LegLocations[] = { FVector(LocalBoundsMax.X - TableLegMeshExtent[0].X,
										LocalBoundsMax.Y - TableLegMeshExtent[0].Y,
										ZCoord),
								FVector(LocalBoundsMax.X - TableLegMeshExtent[1].X,
										LocalBoundsMin.Y + TableLegMeshExtent[1].Y,
										ZCoord),
								FVector(LocalBoundsMin.X + TableLegMeshExtent[2].X,
										LocalBoundsMax.Y - TableLegMeshExtent[2].Y,
										ZCoord),
								FVector(LocalBoundsMin.X + TableLegMeshExtent[3].X,
										LocalBoundsMin.Y + TableLegMeshExtent[3].Y,
										ZCoord) };

	if (TableLegMeshes.Num() == NumLegs)
	{
		for (int i = 0; i < NumLegs; i++)
		{
			TableLegMeshes[i]->SetRelativeLocation(LegLocations[i]);
			//TableLegMeshExtent[i] = ThisTableLegMesh[i]->GetLocalBounds().BoxExtent;
			TableLegMeshExtent[i] = TableLegMeshes[i]->Bounds.BoxExtent;
		}
	}
}

void ATableBase::OnChangeAlongX(UDragAndDropComponent* Dragger, float DeltaX)
{
	UE_LOG(LogTemp, Warning, TEXT("----------------------ATableBase::OnChangeAlongX-----------------"));
	BOUNDS_XY DraggerBounds_XY = DraggersMap[Dragger];
	switch (DraggerBounds_XY)
	{
	case ATableBase::MAX_X_MAX_Y:
	case ATableBase::MAX_X_MIN_Y:
	{
		FVector NewLocalBoundsMax = this->GetLocalBoundsMax();
		NewLocalBoundsMax.X += DeltaX;

		UpdateLocalBoundsMax(NewLocalBoundsMax);
		break; 
	}
	case ATableBase::MIN_X_MAX_Y:
	case ATableBase::MIN_X_MIN_Y:
	{
		FVector NewLocalBoundsMin = this->GetLocalBoundsMin();
		NewLocalBoundsMin.X += DeltaX;

		UpdateLocalBoundsMin(NewLocalBoundsMin);
		break;
	}
	default:
		break;
	}
}

void ATableBase::OnChangeAlongY(UDragAndDropComponent* Dragger, float DeltaY)
{
	UE_LOG(LogTemp, Warning, TEXT("----------------------ATableBase::OnChangeAlongY-----------------"));
	BOUNDS_XY DraggerBounds_XY = DraggersMap[Dragger];
	switch (DraggerBounds_XY)
	{
	case ATableBase::MAX_X_MAX_Y:
	case ATableBase::MIN_X_MAX_Y:
	{
		FVector NewLocalBoundsMax = this->GetLocalBoundsMax();
		NewLocalBoundsMax.Y += DeltaY;

		UpdateLocalBoundsMax(NewLocalBoundsMax);
		break;
	}
	case ATableBase::MAX_X_MIN_Y:
	case ATableBase::MIN_X_MIN_Y:
	{
		FVector NewLocalBoundsMin = this->GetLocalBoundsMin();
		NewLocalBoundsMin.Y += DeltaY;
		
		UpdateLocalBoundsMin(NewLocalBoundsMin);
		break;
	}
	default:
		break;
	}
}



void ATableBase::AddQuadMesh(struct VertexData& MeshVertexData, FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;
	int32 Point4 = TriIndex++;

	MeshVertexData.Vertices.Add(TopRight);
	MeshVertexData.Vertices.Add(BottomRight);
	MeshVertexData.Vertices.Add(TopLeft);
	MeshVertexData.Vertices.Add(BottomLeft);

	MeshVertexData.Triangles.Add(Point1);
	MeshVertexData.Triangles.Add(Point2);
	MeshVertexData.Triangles.Add(Point3);


	MeshVertexData.Triangles.Add(Point4);
	MeshVertexData.Triangles.Add(Point3);
	MeshVertexData.Triangles.Add(Point2);

	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, TopLeft - TopRight).GetSafeNormal();

	for (int i = 0; i < 4; ++i)
	{
		//MeshVertexData.Normals.Add(ThisNorm);
		//MeshVertexData.Tangents.Add(Tangent);
		MeshVertexData.Colors.Add(FLinearColor::Green);
	}

	MeshVertexData.UVs.Add(FVector2D(0.f, 1.f)); // Top left
	MeshVertexData.UVs.Add(FVector2D(0.f, 0.f)); // Bottom Left
	MeshVertexData.UVs.Add(FVector2D(1.f, 1.f)); // Top Right
	MeshVertexData.UVs.Add(FVector2D(1.f, 0.f)); // Bottom Right
}


//#pragma optimize("", off)
void ATableBase::UpdateChairs()
{

	ScreenLog("ATableBase::UpdateChairs");

	if (!ChairPool)
	{
		ScreenLog("ATableBase::UpdateChairs - No Chair Pool, return");
		return;
	}

	FVector TableTopBoundsMax = GetLocalBoundsMax();
	FVector TableTopBoundsMin = GetLocalBoundsMin();

	float TableLengthX = TableTopBoundsMax.X - TableTopBoundsMin.X;
	float TableLengthY = TableTopBoundsMax.Y - TableTopBoundsMin.Y;

	float AvailableTableLengthX = TableLengthX - ((TableLegMeshExtent[0].X * 2) * 2);
	float AvailableTableLengthY = TableLengthY - ((TableLegMeshExtent[0].Y * 2) * 2);

	int32 NumChairsAlongX = AvailableTableLengthX / LengthChairWithMargins;
	int32 NumChairsAlongY = AvailableTableLengthY / LengthChairWithMargins;

	float OffsetX = TableLegMeshExtent[0].X * 2;
	float OffsetY = TableLegMeshExtent[0].Y * 2;

	float ChairStrideX = AvailableTableLengthX / NumChairsAlongX;
	float ChairStrideY = AvailableTableLengthY / NumChairsAlongY;

	int32 IndexChair = 0;
	for (int32 i = 0; i < NumChairsAlongX; i++)
	{
		float CoordX = TableTopBoundsMin.X + OffsetX - (ChairStrideX / 2) + ((i + 1) * ChairStrideX);

		{
			AMyChair* Chair = GetChairAt(IndexChair);
			PlaceChair(Chair, FVector(CoordX, TableTopBoundsMin.Y, 0.0f), FRotator(0.0f, 90.0f, 0.0f));
			IndexChair++;
		}

		{
			AMyChair* Chair = GetChairAt(IndexChair);
			PlaceChair(Chair, FVector(CoordX, TableTopBoundsMax.Y, 0.0f), FRotator(0.0f, -90.0f, 0.0f));
			IndexChair++;
		}
	}

	for (int32 i = 0; i < NumChairsAlongY; i++)
	{
		float CoordY = TableTopBoundsMin.Y + OffsetY - (ChairStrideY / 2) + ((i + 1) * ChairStrideY);

		{
			AMyChair* Chair = GetChairAt(IndexChair);
			PlaceChair(Chair, FVector(TableTopBoundsMin.X, CoordY, 0.0f), FRotator(0.0f, 0.0f, 0.0f));
			IndexChair++;
		}

		{
			AMyChair* Chair = GetChairAt(IndexChair);
			PlaceChair(Chair, FVector(TableTopBoundsMax.X, CoordY, 0.0f), FRotator(0.0f, 180.0f, 0.0f));
			IndexChair++;
		}

	}

	for (int32 i = ChairsInUse.Num() - 1; i >= IndexChair; i--)
	{
		ChairPool->AddReusable(ChairsInUse[i]);
		ChairsInUse.RemoveAt(i, 1, false);
	}
}


AMyChair* ATableBase::GetChairAt(int32 IndexChair)
{
	AMyChair* Chair = nullptr;

	if (ChairsInUse.Num() > IndexChair)
	{
		Chair = ChairsInUse[IndexChair];
	}
	else
	{
		Chair = ChairPool->GetReusable();
		Chair->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		ChairsInUse.Emplace(Chair);
	}

	return Chair;
}

void ATableBase::PlaceChair(AMyChair* Chair, FVector NewLocation, FRotator NewRotation)
{
	if (Chair)
	{
		Chair->SetActorRelativeLocation(NewLocation);
		Chair->SetActorRelativeRotation(NewRotation);
	}
}
